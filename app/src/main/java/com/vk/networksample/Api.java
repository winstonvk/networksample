package com.vk.networksample;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * @author Vladimir Kokhanov
 */
public interface Api {

    @GET("latest")
    Call<ApiResponse> latest(
            @Query("base") String from,
            @Query("symbols") String to
    );
}
